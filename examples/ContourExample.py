
import matplotlib.pyplot as plt
import numpy as np
from peplottools.generalstyling import make_fancy_plot
from peplottools.contourplotting import generate_contours

width, height = plt.rcParams["figure.figsize"]

print(width, height)

def MyTestFunction(xlist: np.array, ylist: np.array):
	return xlist**2 + ylist**2

if __name__ == "__main__":

	fig, ax = plt.subplots(figsize=(width, width))

	## first we create an example table
	Xlist    = np.linspace(-2.0, 2.0, num = 41)
	Ylist    = np.linspace(-2.0, 2.0, num = 41)

	outputXlist = (Xlist[:, None]*np.ones(shape=(len(Xlist), len(Ylist)))).flatten()
	outputYlist = (Ylist[None,:]*np.ones(shape=(len(Xlist), len(Ylist)))).flatten()
	outputZlist = MyTestFunction(outputXlist, outputYlist)

	in_tables = np.array([outputXlist, outputYlist, outputZlist]).transpose()

	## now use the obtained table to generate the contours
	xx = np.unique(in_tables[:, 0])
	yy = np.unique(in_tables[:, 1])
	contours_table  = generate_contours(in_tables, xx, yy)

	## finally we plot the contours
	ax.contour(xx, yy, contours_table, [0, 1.0, 4.0] ,colors= 'black')


	ax.set_xlim([min(Xlist), max(Xlist)])
	ax.set_ylim([min(Ylist), max(Ylist)])


	ax.set_xlabel(r'$x$',  fontsize = 15)
	ax.set_ylabel(r'$y$', fontsize = 15)

	make_fancy_plot(ax)

	plt.tight_layout()
	plt.show()
	plt.close(fig)
	
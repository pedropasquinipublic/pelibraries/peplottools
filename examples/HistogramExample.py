#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				12st Sep 2023					  ### 
#########################################################

import matplotlib.pyplot as plt
import numpy as np
from peplottools.generalstyling import make_fancy_plot
from peplottools.histogramstyle import transform_to_histogram, calc_poisson_error


def MyTestFunction(xlist: np.array):
    return -xlist**2 + 16

if __name__ == "__main__":

    fig, ax = plt.subplots()


    Xedges    = np.linspace(-4.0, 4.0, num = 20)
    Xcentral = 0.5*(Xedges[1:] + Xedges[:-1])

    YCentral = np.array(MyTestFunction(Xcentral))

    YHisto   = transform_to_histogram(YCentral)
    XHisto   = transform_to_histogram(Xedges)[1:-1]

    ax.plot(XHisto, YHisto)

    ax.errorbar(Xcentral, YCentral, calc_poisson_error(YCentral), c = 'black', fmt='o')


    ax.set_xlim([-4.1, 4.1])
    ax.set_ylim([0, 25])


    ax.set_xlabel(r'$x$',  fontsize = 15)
    ax.set_ylabel(r'$f(x)$', fontsize = 15)

    make_fancy_plot(ax)

    plt.tight_layout()
    plt.show()
    plt.close(fig)
#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				25st March 2024					  ### 
#########################################################

import plotly.graph_objects as go
import numpy as np
from peplottools import PlotDisplay


xList = np.linspace(0, 20, num=50)

def create_plot():
    layout = go.Layout(
        plot_bgcolor='rgba(0,0,0,0)',
        font_family = 'serif',
        font_size   = 15,
        yaxis = dict(showline=True, linewidth=2, linecolor='black', mirror=True, title = r"$f(x)$"),
        xaxis = dict(showline=True, linewidth=2, linecolor='black', mirror=True, title = r"$x$" )
    )

    fig = go.Figure(layout = layout)

    fig.add_trace(go.Scatter(x=xList, y=xList**2,
                         mode='lines',
                         line_color='black',
                         name='macd'))

    fig.update_yaxes(range=[0, 100], showline=True, linewidth=2, linecolor='black')

    fig.update_xaxes(range=[min(xList), 0.5*max(xList)])
    
    return fig

auxFig = create_plot()
def Myfunction(**kwargs):

    n = np.random.randint(0, 10)
    auxFig.data[0]["y"] = xList**(n/2)


    return auxFig.to_image(format="png")

if __name__ == "__main__":

    MyWindow = PlotDisplay(Myfunction, refresh_time_ms = 100)

    MyWindow.run()

________________________________________________
<div align="center">
pePlotTools

A python library for styling your plots

Creation date: 21 October 2021          
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>


________________________________________________

# Introduction
This file contains some useful plotting functions I created over the years.

# Instalation

To **install** you can use pip. 

Go to folder in terminal and run 
>> pip3 install -e ../peplottools

To **uninstall** it just type
>> pip3 uninstall peplottools

Obs:. Sometimes you need to install fonts/libraries for rendering LaTeX in python. 
You might need to install the fonts for LaTeX by using

>> sudo apt-get install dvipng texlive-latex-extra texlive-fonts-recommended cm-super

you might also need 

>> sudo apt-get install texlive-fonts-extra

or simply remove/comment the LaTex font style lines from the generalstyling.py file.


# List of interesting functions

> 1. makes the plot looks nicer
>> generalstyling.make_fancy_plot(matplotlib axis)

>  2. help to double the points to create a histogram-like plot 
>> histogramstyle.create_bining(numpy.array)
  
> 3. Function to transform a list into formatting for contour plots
>> contourplotting.generate_contours(data_table, myxx, myyy)

> 4. A class to dynamically display plotly plots.
>> pedynamicdisplay.PlotDisplay


See usage examples in the example files

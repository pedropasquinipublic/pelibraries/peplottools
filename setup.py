from setuptools import setup


setup(
    name='peplottools',
    version='1.1.0',    
    description='Some usefull functions for plotting nice plots',
    url='https://gitlab.com/pedropasquinipublic/pelibraries/pereferences',
    author='Pedro Pasquini',
    author_email='pedrosimpas@gmail.com',
    packages=['peplottools'],
    install_requires=[
                      'numpy',
                      'matplotlib',
                      'PyQt5'              
                      ],

    classifiers=[
        'Intended Audience :: Science/Research', 
        'Operating System :: POSIX :: Linux',      
        'Programming Language :: Python :: 3.8',
    ],
)

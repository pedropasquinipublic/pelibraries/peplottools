##########################################################
###                 created by                         ### 
###                Pedro Pasquini                      ###
###        https://inspirehep.net/authors/1467863      ###
###                                                    ### 
###                 creation date                      ### 
###                 21st Oct 2022                      ### 
##########################################################

from matplotlib.pyplot import Axes
from matplotlib.ticker import AutoMinorLocator
from math import floor, log10, cos, sin


from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif')
#rc('text.latex', preamble=r'\usepackage{stix}')

from matplotlib.lines import Line2D
from matplotlib.path import Path

def latex_float(f: float, precision: int = 3) -> str:
    r'''
        Convert python float into LaTeX style formatting for plotting.    
    '''
    float_str = '%.*g' % (precision, f)
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0} \times 10^{{{1}}}".format(base, int(exponent))
    else:
        return float_str

def list_to_loglist(xmin: float, xmax: float) -> tuple:
    '''
      Transform the ticks labels into a nicely looking ticks labels
    '''
    auxtick=[]
    auxlable=[]
    for i in range(xmin,xmax):
        for j in range (1,10):
            auxtick.append(1.0*j*(10**i))
            if j==1 and i==0:
                auxlable.append(r'$1$')
            elif j==1 and i==1:
                auxlable.append(r'$10$')
            elif j==1:
                auxlable.append(r'$10^{{{}}}$'.format(i))
            else:
                auxlable.append('')
    return  (auxtick, auxlable)

def make_fancy_plot(axis: Axes, NewlabelSize: int = 15) -> None:
    '''
      Function to make the style of the plot look nice. 
    '''

    if axis.get_xscale() == 'log':
        xlimit = axis.get_xlim()

        if xlimit[0] < xlimit[1]:
            auxtick , auxlable = list_to_loglist(int(floor(log10(abs(xlimit[0])))) - 2, int(floor(log10(abs(xlimit[1])))) + 2)

        if xlimit[1] < xlimit[0]:
            auxtick , auxlable = list_to_loglist(int(floor(log10(abs(xlimit[1])))) - 2, int(floor(log10(abs(xlimit[0])))) + 2)
            auxtick  = auxtick[::-1]
            auxlable = auxlable[::-1]

        axis.set_xticks(auxtick)
        axis.set_xticklabels(auxlable)
        axis.set_xlim([xlimit[0], xlimit[1]])
    else:
        axis.xaxis.set_minor_locator(AutoMinorLocator())
        
    if axis.get_yscale() == 'log':
        ylimit = axis.get_ylim()

        if ylimit[0] < ylimit[1]:
            auxtick , auxlable = list_to_loglist(int(floor(log10(abs(ylimit[0])))) - 2, int(floor(log10(abs(ylimit[1])))) + 2)

        if ylimit[1] < ylimit[0]:
            auxtick , auxlable = list_to_loglist(int(floor(log10(abs(ylimit[1])))) - 2, int(floor(log10(abs(ylimit[0])))) + 2)
            auxtick  = auxtick[::-1]
            auxlable = auxlable[::-1]

        axis.set_yticks(auxtick)
        axis.set_yticklabels(auxlable)
        axis.set_ylim([ylimit[0], ylimit[1]])
    else:
        axis.yaxis.set_minor_locator(AutoMinorLocator())

    axis.tick_params(which='major', length=6, width = 0.5, top = True, right = True,direction = 'in', zorder = 100)
    axis.tick_params(which='minor', length=3, width = 0.5, top = True, right = True,direction = 'in', zorder = 100)
    axis.tick_params(labelsize=NewlabelSize)


def _GetErrorBArHandler():
    mycircle = [[0.4*cos(2.0*3.14159*x/20), 0.4*sin(2.0*3.14159*x/20)] for x in range(20)]
    vertices = [[0,-1], [0,1]] + mycircle + [[-0.5, 1], [0.5,1]] + [[-0.5, -1], [0.5,-1]]
    codes    = [Path.MOVETO, Path.LINETO, Path.MOVETO] + [Path.LINETO]*(len(mycircle)-2) + [Path.CLOSEPOLY] + [Path.MOVETO, Path.LINETO, Path.MOVETO, Path.LINETO]
    return Path(vertices, codes)


def AddExtraLegend(axis, label_list: list, color_list: list, ls_list: list, loc : tuple, frameon=False, NewlabelSize = 15):
    '''
        Add a extra legend to the plot.
          marker_list elements should be 
           "-", "--", "-.", ":", "band", "BF", "point" or "errorpoint"
    '''
    _style_dict = {"-"          : ("-", 2, 1.0, None, 1), 
                   "--"         : ("--", 2, 1.0, None, 1), 
                   "-."         : ("-.", 2, 1.0, None, 1), 
                   ":"          : (":", 2, 1.0, None, 1), 
                   "band"       : ("-", 10, 0.5, None, 1), 
                   "BF"         : ("-", 0, 1.0, "*", 8), 
                   "point"      : ("-", 0, 1.0, "o", 8), 
                   "errorpoint" : ("-", 0, 1.0, _GetErrorBArHandler(), 13)
                  }
    
    my_style = map(lambda x: _style_dict[x] if x in _style_dict.keys() else (x, 1, 1.0, None), ls_list)

    _custom_lines = [
        Line2D([0], [0], color = color, ls = _style[0], lw=_style[1], alpha = _style[2] , marker = _style[3], ms = _style[4]*NewlabelSize/15.0)
        for color, _style in zip(color_list, my_style)
        ]
        
    axis.add_artist(axis.legend(_custom_lines, label_list,\
                    loc= loc,frameon = frameon, fontsize = NewlabelSize))

"""
       -- pePlotTools -- 

Some usefull functions for plotting nice plots
"""

# creation date 21st Oct 2022
__version__ = "1.1.0"
__author__ = 'Pedro Pasquini'
__credits__ = 'https://inspirehep.net/authors/1467863'

from peplottools.pedynamicdisplay import PlotDisplay
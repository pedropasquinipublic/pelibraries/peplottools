#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				21st Oct 2022					  ### 
#########################################################

import numpy as np
from scipy.interpolate import griddata

def _rearange_table(xlist, ylist, function, grided = False):
	'''
	  Rearange a table to make it in the format used by 
	  plot tool
	'''
	returnZ=np.zeros((len(xlist),len(ylist)))

	if not grided:
		for j in range(len(ylist)):
			for i in range(len(xlist)):
				returnZ[j,i]=function(xlist[i], ylist[j])
	else:
		count=0
		for i in range(len(xlist)):
			for j in range(len(ylist)):
				returnZ[i,j]=function[count]
				count=count+1	

	return returnZ.T

### This function is ugly, but is what I did long time ago. In the future I should make it better/faster and avoid using scipy...
def generate_contours(data_table, myxx, myyy, is_logx = False, is_logy = False, interp_method = 'linear'): 
    '''
    This function get a 2D chi2 table and 
    transforms it into suitable form for plotting
    2D contours.\n
    Possible interp_methods:\n
    'linear', 'nearest', 'cubic'
    '''

    xpoints=np.zeros(len(myyy)*len(myxx))
    ypoints=np.zeros(len(myyy)*len(myxx))

    count=0
    for i in range(0,len(myxx)):
        for j in range(0,len(myyy)):
            xpoints[count]=myxx[i]
            ypoints[count]=myyy[j]
            count = count+1

    _list = [[],[],[]]

    for line in data_table:
        if is_logx:
            _list[0].append(np.log10(line[0]))
        else:
            _list[0].append(line[0])
        if is_logy:
            _list[1].append(np.log10(line[1]))
        else:
            _list[1].append(line[1])

        _list[2].append(line[2])

    func = griddata((_list[0],_list[1]), _list[2], (xpoints, ypoints), method = interp_method )

    return _rearange_table(myxx, myyy, func, grided = True)

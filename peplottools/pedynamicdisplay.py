from typing import Callable
import sys
from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap
import PyQt5.QtCore



def _definePosition(screen_height : float, screen_width: float, pos: tuple[int, int, int, int]):
    r'''
      Transform the tuple pos = (pos_x, pos_y, size_z, size_y)
      to the position relative to the screen variables.
    '''

    return (int(pos[0]*screen_width), int(pos[1]*screen_height), int(pos[2]*screen_width), int(pos[3]*screen_height))

def _CreateButtom(inPlotDisplay: QtWidgets.QMainWindow, label : str, pos : tuple[int, int, int, int], func: Callable) -> None:
    r'''
     Function to create Buttons.

     pos = (pos_x, pos_y, size_z, size_y) is relative to the screen heigh and width.
    '''
    # creating a push button
    setattr(inPlotDisplay, label, QtWidgets.QPushButton(label, inPlotDisplay))

    # setting geometry of button
    getattr(inPlotDisplay, label).setGeometry(*pos)
    getattr(inPlotDisplay, label).clicked.connect(func)

class PlotDisplay(QtWidgets.QMainWindow):
    r'''
      A class to display and update plotly plots.

      The variable update_function : Callable
      should be an update-like function that receives the class kwargs 
      and returns a bitmap object that will be displayed by the class.
    '''
    UpdateFunction  : Callable 
    PixelMap        : QPixmap
    app             : QtWidgets.QApplication
    plot            : QtWidgets.QLabel
    screen_height   : int
    screen_width    : int
    refresh_time_ms : float

    def __init__(self, update_function: Callable[[dict], bytes], title = '', auto_refresh = True, refresh_time_ms = 500, bkgcolor = (205, 215,226), **kwargs) -> None:

        self.app = QtWidgets.QApplication(sys.argv)

        width,  height     = self.app.primaryScreen().size().width(),  self.app.primaryScreen().size().height()
        ScreenSize         = min(width, height)
        self.screen_height = int(0.5*ScreenSize)
        self.screen_width  = int(1.333333*self.screen_height)

        super().__init__() 
        self.setContentsMargins(0, 0, 0, 0)
        self.setGeometry(self.screen_width//2, self.screen_height//2, self.screen_width, self.screen_height)
        self.setWindowTitle(title)
        self.setStyleSheet(f"background-color: rgb({bkgcolor[0]}, {bkgcolor[1]}, {bkgcolor[2]});") 
        

        ## List of buttons. Should be pos relative to the screen and function ##
        self.ButtonsDict = {
            "Start": [(0.10, 0.05, 0.10, 0.05)  , self.StartRefresh],
            "Stop" : [(0.21, 0.05, 0.1, 0.05)   , self.StopRefresh],
        }

        if type(refresh_time_ms) == int or type(refresh_time_ms) == float:
            self.refresh_time_ms = float(refresh_time_ms)
        else:
            raise TypeError("refresh_time_ms should be a integer or a float.")

        self.UpdateFunction = update_function
        self.auto_refresh = auto_refresh
        self.kwargs       = kwargs

        self.timer        = PyQt5.QtCore.QTimer(self, interval = refresh_time_ms, timeout = self.handle_timeout)

        self.plot          = QtWidgets.QLabel(parent = self, alignment= PyQt5.QtCore.Qt.AlignCenter)
        self.plot.setGeometry(int(0.025*self.screen_width),  int(0.025*self.screen_height), int(0.95*self.screen_width), int(0.95*self.screen_height))
        self.PixelMap     = QPixmap()
        self.setCentralWidget(self.plot)

        self._UiComponents()

    def _UiComponents(self):
        r'''
          Generate buttons from the button dictionary.
        '''
        for labels, info in self.ButtonsDict.items():
            _CreateButtom(self, labels, _definePosition(self.screen_height, self.screen_width, info[0]), info[1])

    def _AutoRefresh(self) -> None:
        r'''
          Starting the timer.
        '''
        self.timer.start()
        self.handle_timeout()

    def StopRefresh(self) -> None:
        r'''
          Stop refreshing the plot function
        '''
        self.timer.stop()

    def StartRefresh(self, refresh_time_ms = False) -> None:
        r'''
          Start refreshing the plot function
        '''
        if refresh_time_ms:
            self.refresh_time_ms = refresh_time_ms
            self.timer.setInterval(self.refresh_time_ms)
        self._AutoRefresh()

    def handle_timeout(self) -> None:
        r'''
          Function to be called by timer for updating the plot.
        '''
        self.PixelMap.loadFromData(self.UpdateFunction(**self.kwargs))
        self.plot.setPixmap(self.PixelMap)
        self.plot.update()
        self.update()

    def run(self) -> None:
        r'''
         Start running the function.
        '''
        self.handle_timeout()
        self.show()
        if self.auto_refresh:
            self._AutoRefresh()
        self.app.exit(self.app.exec_())
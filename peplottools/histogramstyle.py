#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				21st Oct 2022					  ### 
#########################################################

import numpy as np

def calc_poisson_error(Ylimit: float) -> np.array:
	'''
	  Calculates the Error of a Poisson based histogram for any value of events, even if N is small.
	  It converges to the usual sqrt(N) for large N.
	  See https://www-cdf.fnal.gov/physics/statistics/notes/pois_eb.txt
	  See also 1903.07185
	'''
	return np.array([-0.5 + np.sqrt(Ylimit + 0.25), 0.5 + np.sqrt(Ylimit + 0.25)])

def transform_to_histogram(array: list, add_zero: bool = True) -> np.array:
	'''
	  Transform the array into a histogram-like array.
	'''

	if add_zero:
		return np.append(np.append(0,np.array([array, array]).transpose().flatten()),0)
	return np.array([array, array]).transpose().flatten()

def create_bining(binsize_list: np.array, ini_value: float = 0.0)-> np.array:
	'''
	  Given a list of bin sizes, creates the bining necessary to plot the histogram.
	'''
	return_array = ini_value + np.cumsum(binsize_list)
	return_array = np.insert(return_array, 0, ini_value)
	return_array = transform_to_histogram(return_array)

	return return_array
